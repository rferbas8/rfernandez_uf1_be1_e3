# Lectura i escriptura de dades. <br><br>

#### **Exercici 1:** Escriu un missatge de benvinguda per pantalla. <br><br>

```c
#include <stdio.h>
#include <stdlib.h>

int main()
{
  printf("Benbinguts\n");  
   
 return 0;
}
```

![Imatge](Imatge/cap1.png)<br><br>

#### **Exercici 2:** Escriu un enter, un real, i un caràcter per pantalla.<br><br>

```c
#include <stdio.h>
#include <stdlib.h>

int main()
{
    //varables
    int enter;
    int enter2;
    double realDoblePrecisio;
    char caracter;


    //inicialitzar
    enter=43;
    realDoblePrecisio=3.14;
    caracter='A';

    //Exercici 3
printf("enter: %i\n, double: Escriu un enter, un real, i un caràcter per pantalla.%lf, caracter: %c\n", enter,realDoblePrecisio,caracter);

  return 0;
}
```

![Imatge](/Imatge/cap2.png)<br><br>


#### **Exercici 3:** Demana dos enters per teclat i mostra la suma per pantalla.<br><br>

```c
#include <stdio.h>
#include <stdlib.h>

int main()
{
    //varables
    int enter,enter2;

    //Exercici 3

printf("Intro 1r enter a sumar: ");
  scanf("%i",&enter);
printf("Intro 2n enter a sumar: ");
  scanf("%i",&enter2);
printf("Resultat %i",enter+enter2);
    return 0;
}
```
![Imatge](/Imatge/cap3.png)<br><br>


#### **Exercici 4:** Indica el tipus de dades que correspon en cada cas:<br><br>

<div style="color:#90b9ff">

- Edat: Enter int.
- Temperatura. Real doble.
- La resposta a la pregunta: Ets solter (s/n)? Booleans if.
- El resultat d’avaluar la següent expressió: (5>6 o 4<=8)
- El teu nom. Caracter char.
<br><br>
</div>

#### **Exercici 5:** Mostrar per pantalla els 5 primers nombres naturals.<br><br>

```c
#include <stdio.h>
#include <stdlib.h>

int main()
{
    //Exercici 5
    printf("1,2,3,4,5");

    return 0;
}
```

![Imatge](/Imatge/cap5.png)<br><br>


#### **Exercici 6:** Mostrar per pantalla la suma i el producte dels 5 primers nombres naturals.<br><br>

```c
#include <stdio.h>
#include <stdlib.h>

int main()
{
    //varables


    //Exercici 6
    printf("La suma: %i, el producte: %i",1+2+3+4+5,1*2*3*4*5);



    return 0;
}
```
![Imatge](/Imatge/cap5.png)<br><br>

#### **Exercici 7:** Demana dos enters...<br><br>

```c
#include <stdio.h>
#include <stdlib.h>

int main()
{
    //variables
int a,b,tmp;


//inicialitzar
printf("Introdueix el valor que tindra a: ");
    scanf(" %i",&a);
printf("Introdueix el valor que tindra b: ");
    scanf("%i",&b);

//Exercici 6
printf("La variable a es %i, i el valor de b es %i",a,b);


    return 0;
}
```

![Imatge](/Imatge/cap7.png)<br><br>


#### ...i intercanvia els valors de les variables:<br><br>

```c
#include <stdio.h>
#include <stdlib.h>

int main()
{
    //variables
int a,b,tmp;

//inicialitzar
printf("Introdueix el valor que tindra a: ");
    scanf(" %i",&a);
printf("Introdueix el valor que tindra b: ");
    scanf("%i",&b);

tmp=a;
a=b;
b=tmp;

//Exercici 6
printf("La variable a es %i, i el valor de b es %i",a,b);

    return 0;
}
```
![Imatge](/Imatge/cap72.png)<br><br>

#### **Exercici 8:** Mostrar per pantalla el següent menú:<br><br>

```c
#include <stdio.h>
#include <stdlib.h>

int main()
{
    printf("Opcions:\n ");
        printf("\n\t1-Alta\n");
        printf("\t2-Baixa\n");
        printf("\t3-Modificacions\n");
        printf("\t4-Sortir\n");
    printf("\nTria opció (1-4)");
    return 0;
}
```

![Imatge](/Imatge/cap8.png)<br><br>

#### **Exercici 9:** Mostra per pantalla el següent menú:<br><br>

```c
#include <stdio.h>
#include <stdlib.h>

int main()
{
  printf("***************************\n");
  printf("*****\tMENU\t*****\n");
  printf("***************************\n");
    printf("****\t1-alta\t****\n");
    printf("****\t2-baixa\t****\n");
    printf("****\t2-sortir\t****\n");
  printf("***************************\n");


    return 0;
}
```

![Imatge](/Imatge/cap9.png)<br><br>


#### **Exercici 10:** Feu un programa que resolgui el següent enunciat: Calcular el Capital Final CF d’una inversió, a interès simples, si sabem el Capital Inicial (C0), el nombre d’anys (n), i el rèdit (r). La fórmula per obtenir el CF a interès simples és:<br><br>
![Imatge](/Imatge/acap10.png)<br><br>
Donat que el temps està expressat en anys la k=1.
El rèdit ‘r’ ve donat en %.
Nota: les dades de l’exercici cal demanar-les per teclat.

```c
#include <stdio.h>
#include <stdlib.h>

int main()
{
int entrada[3];
int capiralInicial, redit, t;
float resultat;


  printf("Introdueix el valor que tindra C0:");
        scanf("%i",&entrada[0]);
  printf("Introdueix el valor que tindra r:");
        scanf("%i",&entrada[1]);
  printf("Introdueix el valor que tindra t:");
        scanf("%i",&entrada[2]);

if(capiralInicial==entrada[0]);
if(redit==entrada[1]);
if(t==entrada[2]);

printf("La teva combinacó de numeros es: %i, %i, %i, \n",entrada[0],entrada[1],entrada[2]);

    resultat = entrada[0] + ((entrada[0] * entrada[1] * entrada[2]) / 100 * 1);

printf("El resultat de el Capital Final es:%f ",resultat);


    return 0;
}
```
![Imatge](/Imatge/cap10.png)<br><br>


<br><br>

#### **Exercici 11:** Feu un programa que resolgui el següent enunciat: Calcular el Capital Final CF d’una inversió, a interès compost, si sabem el Capital Inicial (C0), el nombre d’anys (n), i el interès (i). La fórmula per obtenir el CF a interès compost és:<br><br>

![Imatge](/Imatge/acap11.png)<br><br>

L’ interès ‘i’ ve donat en tant per 1.
Nota: les dades de l’exercici cal demanar-les per teclat.<br><br>

  ```c
  #include <stdio.h>
#include <stdlib.h>
#include <math.h>
int main()
{
int entrada[3];
int capiralInicial,i,n;
float resultat;


  printf("Introdueix el valor que tindra C0:");
        scanf("%i",&entrada[0]);
 printf("Introdueix el valor que tindra i: ");
        scanf("%i",&entrada[1]);
 printf("Introdueix el valor que tindra n: ");
        scanf("%i",&entrada[2]);


if(capiralInicial==entrada[0]);
if(i==entrada[1]);
if(n==entrada[2]);

printf("La teva combinacó de numeros es: %i, %i, %i, \n",entrada[0],entrada[1],entrada[2]);

resultat=entrada[0]*pow(1+entrada[1],entrada[2]);

printf("El resultat de el Capital Final es:%f ",resultat);


    return 0;
}
```

![Imatge](/Imatge/cap11.png)<br><br>

**Tot i que de aquesta manera nomes funciona en windows.
Ja que en Linux s'hauria de indicar el literal.**



#### **Exercici 12:** Feu un programa que permeti generar una travessa de forma aleatòria.Tot seguit ha de permetre introduir-ne una per teclat i comprovar-ne els encerts respecte la travessa generada aleatòriament. El format de sortida queda a la vostra elecció sempre que n’informi dels encerts.<br><br>

(preguntar classe)<br><br>



#### **Exercici 13:** Feu un programa que permeti simular el resultat de tirar un dau. Tot seguit ha de demanar un número per teclat, entre 1 i 6, i ens ha dir si és el mateix que el que hem generat aleatòriament.<br><br>

```c
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main()
{
    //variables
int resultat;
double alea[6];
int entrada[6];
//llabor da la funcio rand
        srand(getpid());


//inicialitza
resultat=0;


 //Genera numeros aleatoris

    alea[0]= rand()%6;


    printf("Introdueix un numero de el 1 al 6: ");
    scanf("%i",&entrada[0]);


//Comprova resultats
if(alea[0]==entrada[0]) resultat++;


printf("Has endevinat %i número",resultat);
printf("El numero que ha tocat del dau es:%.0lf, \n", alea[0]);
printf("El numero que havies sellecionat era: %i \n",entrada[0]);



    return 0;
}
```

![Imatge](/Imatge/cap13.png)<br><br>
